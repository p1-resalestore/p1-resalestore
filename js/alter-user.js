

//removing item fields
document.getElementById("remove-user").addEventListener("click",function(e){
    e.preventDefault();
        document.getElementById("id").hidden = false;
        document.getElementById("user-role").hidden = true;
        document.getElementById("user-fn").hidden = true;
        document.getElementById("user-ln").hidden = true;
        document.getElementById("user-address").hidden = true;
        document.getElementById("user-email").hidden = true;
        document.getElementById("update-user-submit").hidden = true;
        document.getElementById("remove-user-submit").hidden = false;
})
//updateing item fields
document.getElementById("update-user").addEventListener("click",function(e){
    e.preventDefault();
    document.getElementById("id").hidden = false;
    document.getElementById("user-role").hidden = false;
    document.getElementById("user-fn").hidden = false;
    document.getElementById("user-ln").hidden = false;
    document.getElementById("user-address").hidden = false;
    document.getElementById("user-email").hidden = false;
    document.getElementById("update-user-submit").hidden = false;
    document.getElementById("remove-user-submit").hidden = true;
})

//remove item
document.getElementById("remove-user-submit").onclick = removeUser;
function removeUser(e){
    e.preventDefault();
    
const id = document.getElementById("user-id").value;

fetch( "http://localhost:7080/users", {
    method:"DELETE",
    headers: {
        "Content-type": "application/json; charset=UTF-8"
    },    
    body: id
})     
.then(
    document.getElementById("alter-item-msg").hidden=false,
    console.log("item removed"))
.catch(error => console.log("ERROR"));
}


//update item
document.getElementById("update-user-submit").onclick = updateItem;
function updateItem(e){
    e.preventDefault();

const id = document.getElementById("user-id").value;
const role = document.getElementById("role").value;
const firstname = document.getElementById("firstname").value;
const lastname = document.getElementById("lastname").value;
const address = document.getElementById("address").value;
const email = document.getElementById("email").value;

    fetch( "http://localhost:7080/users", {
        method:"PATCH",
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },    
        body:JSON.stringify({
            userId:id,
            userRole:role,
            firstname:firstname,
            lastname:lastname,
            address:address,
            email:email
        })  
    })   
    .then(
        document.getElementById("alter-user-msg").hidden=false,
        console.log(body))
    .catch(error => console.log("ERROR")) 
}
