sendAjaxGet("http://localhost:7080/items", undefined, successfullCallBack, failurCallBack, undefined)

function renderItemsInTable(itemsList){
    const table = document.getElementById("items-table-body");
    for(let items of itemsList){
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${items.itemId}</td><td><img style="hight:5vw; width:5vw" src=${items.picture}></td><td>${items.itemName}</td><td><textarea readonly id="item-description" class="form-control" aria-label="With textarea">${items.itemDescription}</textarea></td><td>${items.itemCategory}</td><td>${items.itemCondition}</td><td>${items.itemQuantity}</td><td>${items.itemPrice}</td>`;
        table.appendChild(newRow);
    }

}
function successfullCallBack(xhr){
    const inventory = JSON.parse(xhr.responseText)
    renderItemsInTable(inventory);
}
function failurCallBack(xhr){
console.log("something didnt go right");
}
