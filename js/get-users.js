sendAjaxGet("http://localhost:7080/users", undefined, successfullCallBack, failurCallBack, undefined)

function renderItemsInTable(userList){
    const table = document.getElementById("users-table-body");
    for(let users of userList){
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${users.userId}</td><td>${users.userRole}</td><td>${users.username}</td><td id="pass-field">${users.userpass}</td><td>${users.firstname}</td><td>${users.lastname}</td><td>${users.address}</td><td>${users.email}</td>`;
        table.appendChild(newRow);
    }

}
function successfullCallBack(xhr){
    const users = JSON.parse(xhr.responseText)
    renderItemsInTable(users);
}
function failurCallBack(xhr){
console.log("something didnt go right");
}